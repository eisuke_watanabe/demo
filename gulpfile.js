var gulp = require('gulp');
var glob = require('glob');
var plumber = require('gulp-plumber');
var browser = require("browser-sync");
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var imageOptim = require('gulp-imageoptim');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var csso = require('gulp-csso');
var ejs = require('gulp-ejs');



gulp.task('server', function() {
    browser({
        server: {
            baseDir: './build/'
        }
    });
});

// 画像の軽量化
gulp.task('imagemin', function(){
    gulp.src('./src/images/*.png')
        .pipe(imageOptim.optimize())
        .pipe(gulp.dest('./build/images'));
});


// sass
gulp.task('css', function() {
    gulp.src('./src/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(sass())
        .pipe(postcss([
            autoprefixer({browsers: ['android 2.3', 'ios 4']})
        ]))
        .pipe(cleanCSS())
        .pipe(csso())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./build/css'))
        .pipe(browser.reload({stream:true}));
});


// template
gulp.task('html', function() {
    gulp.src(['./src/html/pages/*.ejs', '!./src/html/**/_*.ejs'])
        .pipe(ejs())
        .pipe(gulp.dest('./build'))
		.pipe(browser.reload({stream:true}));
});


// build
gulp.task('build', ['html', 'css', 'imagemin']);


// 開発時に実行
gulp.task('default', ['server'], function(){
    gulp.watch('./src/sass/**/*.scss', ['css']);
	gulp.watch('./src/html/**/*.ejs', ['html']);
});